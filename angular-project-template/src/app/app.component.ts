import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { Product } from './models/product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  calendarValue: Date;
  products: Product[];

  constructor(private config: PrimeNGConfig) {}

  ngOnInit() {
    this.config.setTranslation({
      dayNames: ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'],
      dayNamesShort: [
        "יום 'א",
        "יום 'ב",
        "יום 'ג",
        "יום 'ד",
        "יום 'ה",
        "יום 'ו",
        "יום 'ש",
      ],
      dayNamesMin: ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'],
      monthNames: [
        'ינואר',
        'פברואר',
        'מרץ',
        'אפריל',
        'מאי',
        'יוני',
        'יולי',
        'אוגוסט',
        'ספטמבר',
        'אוקטובר',
        'נובמבר',
        'דצמבר',
      ],
      monthNamesShort: [
        'ינו',
        'פבר',
        'מרץ',
        'אפר',
        'מאי',
        'יונ',
        'יול',
        'אוג',
        'ספט',
        'אוק',
        'נוב',
        'דצמ',
      ],
      today: 'היום',
      clear: 'נקה',
    });
  }
}
