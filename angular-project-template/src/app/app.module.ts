import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';

import { AppComponent } from './app.component';

// PrimeNG Modules
import { CalendarModule } from 'primeng/calendar';
import { SharedModule } from 'primeng/api';
import { FormsModule } from '@angular/forms';
import { PopulationsComponent } from './pages/populations/populations.component';

import { PopulationService } from './pages/populations/population.service';

@NgModule({
  declarations: [AppComponent, PopulationsComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,

    // PrimeNG
    SharedModule,
    CalendarModule,
    DataViewModule,
    TableModule
  ],
  providers: [PopulationService],
  bootstrap: [AppComponent],
})
export class AppModule { }
