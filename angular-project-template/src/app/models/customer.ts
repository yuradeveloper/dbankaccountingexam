export interface Customer {
    CustomerID: number;
    CustomerName: string;
    Title: string;
}