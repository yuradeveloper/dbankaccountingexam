export interface Product {
    ProductID?: number;
    Name?: string;
    ProductNumber?: string;
}