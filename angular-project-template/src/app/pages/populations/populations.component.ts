import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Customer } from 'src/app/models/customer';
import { PopulationService } from './population.service';

import { fromEvent, of, Subscription } from "rxjs";
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter
} from "rxjs/operators";

@Component({
  selector: 'app-populations',
  templateUrl: './populations.component.html',
  styleUrls: ['./populations.component.scss']
})
export class PopulationsComponent implements OnInit {

  constructor(private populationService: PopulationService) { }

  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;

  populations: Customer[];

  formCtrl: Subscription;

  ngOnInit(): void {

    fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      map((event: any) => {
        return event.target.value;
      })
      , filter(res => res.length > 2)
      , debounceTime(1000)
      , distinctUntilChanged()
    ).subscribe((search: string) => {

      this.populationService.GetPopulationFilter(search).subscribe((res) => {
        console.log('res', res);
        this.populations = res;
      }, (err) => {
        console.log('error', err);
      });
    });

  }

  ngOnDestroy() {
    this.formCtrl.unsubscribe();
  }

}
