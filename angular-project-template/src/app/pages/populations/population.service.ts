import { Injectable } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopulationService {

  constructor(private http: HttpClient) { }

  public GetPopulationFilter(customerName: string): any {

    if (customerName === '') {
      return of([]);
    }

    return this.http.get<any>(environment.BaseUrl + 'GetPopulationFilter')
      .toPromise()
      .then(res => { return <Customer[]>res; });
  }

}
