﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DbankAccountingExam.Models;
using DbankAccountingExam.Persistence;
using Microsoft.EntityFrameworkCore;

namespace DbankAccountingExam.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;

        public ProductController(ILogger<ProductController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IList<Product> Get()
        {
            IList<Product> products;

            using (var context = new ProductContext())
            {
                products = context.Products.FromSqlRaw($"SELECT [ProductId], [Name], [ProductNumber] FROM [SalesLT].[Product]").ToList();
            }

            return products;
        }
    }
}
