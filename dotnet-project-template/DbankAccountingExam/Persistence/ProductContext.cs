using DbankAccountingExam.Models;
using Microsoft.EntityFrameworkCore;

namespace DbankAccountingExam.Persistence
{
    public class ProductContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=sqlservercentralpublic.database.windows.net;Database=AdventureWorks;User Id=sqlfamily;Password=sqlf@m1ly;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(p => p.ProductID);

            modelBuilder.Entity<Customer>().HasKey(c => c.CompanyName);
        }
    }
}